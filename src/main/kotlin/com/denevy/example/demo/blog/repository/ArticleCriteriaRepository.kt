package com.denevy.example.demo.blog.repository

import com.denevy.example.demo.blog.dto.ArticleListRequest
import com.denevy.example.demo.blog.dto.PagedArticles

interface ArticleCriteriaRepository {

    fun listPagedArticles(listRequest: ArticleListRequest): PagedArticles
}
