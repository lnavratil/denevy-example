package com.denevy.example.demo.blog.dto

import com.denevy.example.demo.blog.entity.Article

data class ArticleRequest(
        val title: String,
        val content: String,
        val authorId: Int
)

data class ArticleResponse(
        val id: Long,
        val title: String,
        val content: String,
        val authorId: Int
)


data class ArticleListRequest(
        val filter: ArticleListFilter,
        val page: Int = 1,
        val countPerPage: Int = 10
)

data class ArticleListFilter(
        val titleContains: String? = null,
        val authorId: Int? = null,
)

data class ArticleListResponse(
        val articles: List<Article>,
        val page: Int,
        val countPerPage: Int,
        val totalCount: Long
)
