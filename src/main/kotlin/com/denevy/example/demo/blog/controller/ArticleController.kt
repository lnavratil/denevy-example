package com.denevy.example.demo.blog.controller

import com.denevy.example.demo.blog.dto.ArticleListFilter
import com.denevy.example.demo.blog.dto.ArticleListRequest
import com.denevy.example.demo.blog.dto.ArticleRequest
import com.denevy.example.demo.blog.exception.ArticleException
import com.denevy.example.demo.blog.service.ArticleService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/blog/articles")
@RestController
class ArticleController(
        private val articleService: ArticleService
) {

    @PostMapping
    fun addArticle(@RequestBody articleDto: ArticleRequest) = handleResponse {
        articleService.addArticle(articleDto)
    }

    @GetMapping("/{articleId}")
    fun listArticles(@PathVariable("articleId") articleId: Long) = handleResponse {
        articleService.getArticle(articleId)
    }

    @GetMapping
    fun listArticles(
            @RequestParam(value = "filterTitleContains", required = false) filterTitleContains: String?,
            @RequestParam(value = "filterAuthorId", required = false) filterAuthorId: Int?,
            @RequestParam(value = "page", required = true) page: Int,
            @RequestParam(value = "countPerPage", required = true) countPerPage: Int,
    ) = handleResponse {
        val articleListRequest = ArticleListRequest(
                filter = ArticleListFilter(filterTitleContains, filterAuthorId),
                page = page,
                countPerPage = countPerPage
        )
        articleService.listArticles(articleListRequest)
    }

    private fun <T> handleResponse(block: () -> T): ResponseEntity<*> =
            try {
                val result = block()
                ResponseEntity(result, HttpStatus.OK)
            } catch (ex: ArticleException) {
                when (ex) {
                    is ArticleException.ArticleProcessException -> ResponseEntity("Error during process request", null, HttpStatus.INTERNAL_SERVER_ERROR)
                    is ArticleException.ArticleNotFoundException -> ResponseEntity("Article not found", null, HttpStatus.NOT_FOUND)
                    is ArticleException.ArticleBadRequestException -> ResponseEntity("Bad request", null, HttpStatus.BAD_REQUEST)
                }
            } catch (ex: Exception) {
                ResponseEntity("Unexpected exception.", null, HttpStatus.INTERNAL_SERVER_ERROR)
            }

}
