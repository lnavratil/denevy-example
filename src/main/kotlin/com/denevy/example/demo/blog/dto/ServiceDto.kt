package com.denevy.example.demo.blog.dto

import com.denevy.example.demo.blog.entity.Article

data class PagedArticles(
        val articles: List<Article>,
        val totalCount: Long
)
