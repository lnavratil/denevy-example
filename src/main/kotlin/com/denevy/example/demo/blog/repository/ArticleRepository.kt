package com.denevy.example.demo.blog.repository

import com.denevy.example.demo.blog.entity.Article
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ArticleRepository : CrudRepository<Article, Long>, ArticleCriteriaRepository


