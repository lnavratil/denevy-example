package com.denevy.example.demo.blog.dto

import com.denevy.example.demo.blog.entity.Article

fun ArticleRequest.toArticleEntity(): Article {
    val dto = this
    return Article().apply {
        title = dto.title
        content = dto.content
        authorId = dto.authorId
    }
}

fun Article.toArticleResponseDto(): ArticleResponse {
    val entity = this
    return ArticleResponse(
            id = entity.id,
            title = entity.title,
            content = entity.content,
            authorId = entity.authorId
    )
}
