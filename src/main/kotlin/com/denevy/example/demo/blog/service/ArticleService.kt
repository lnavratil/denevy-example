package com.denevy.example.demo.blog.service

import com.denevy.example.demo.blog.dto.ArticleListRequest
import com.denevy.example.demo.blog.dto.ArticleListResponse
import com.denevy.example.demo.blog.dto.ArticleRequest
import com.denevy.example.demo.blog.dto.ArticleResponse
import com.denevy.example.demo.blog.dto.toArticleEntity
import com.denevy.example.demo.blog.dto.toArticleResponseDto
import com.denevy.example.demo.blog.exception.ArticleException
import com.denevy.example.demo.blog.exception.ArticleException.ArticleProcessException
import com.denevy.example.demo.blog.repository.ArticleRepository
import org.springframework.stereotype.Service

@Service
class ArticleService(
        private val articleRepository: ArticleRepository
) {

    fun getArticle(articleId: Long): ArticleResponse =
            articleRepository.findById(articleId).orElseGet {
                throw ArticleException.ArticleNotFoundException("Article with id '$articleId' not found.")
            }.toArticleResponseDto()


    fun listArticles(articleListRequest: ArticleListRequest): ArticleListResponse =
            try {
                val pagedArticles = articleRepository.listPagedArticles(articleListRequest)
                ArticleListResponse(
                        articles = pagedArticles.articles,
                        page = articleListRequest.page,
                        countPerPage = articleListRequest.countPerPage,
                        totalCount = pagedArticles.totalCount
                )
            } catch (ex: Exception) {
                throw ArticleProcessException("Cannot list articles.", ex)
            }


    fun addArticle(newArticle: ArticleRequest) {
        validNewArticle(newArticle)
        articleRepository.save(newArticle.toArticleEntity())
    }

    private fun validNewArticle(newArticle: ArticleRequest) {
        if (newArticle.title.isBlank() || newArticle.content.isBlank() || newArticle.authorId < 1) {
            throw ArticleException.ArticleBadRequestException("Request for adding article has invalid content. Bad request: $newArticle")
        }
    }

}
