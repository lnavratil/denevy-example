package com.denevy.example.demo.blog.repository

import com.denevy.example.demo.blog.dto.ArticleListFilter
import com.denevy.example.demo.blog.dto.ArticleListRequest
import com.denevy.example.demo.blog.dto.PagedArticles
import com.denevy.example.demo.blog.entity.Article
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

@Repository
class ArticleCriteriaRepositoryImpl(
        private val em: EntityManager
) : ArticleCriteriaRepository {


    override fun listPagedArticles(listRequest: ArticleListRequest): PagedArticles {
        return PagedArticles(
                articles = listArticles(listRequest),
                totalCount = totalCount(listRequest)
        )
    }

    private fun listArticles(listRequest: ArticleListRequest): List<Article> {
        val cb = em.criteriaBuilder
        val query = cb.createQuery(Article::class.java)
        val rootArticle = query.from(Article::class.java)
        applyFilter(cb, rootArticle, query, listRequest.filter)

        val typedQuery = em.createQuery(query)

        typedQuery.firstResult = (listRequest.page - 1) * listRequest.countPerPage
        typedQuery.maxResults = listRequest.countPerPage

        return typedQuery.resultList
    }

    private fun totalCount(listRequest: ArticleListRequest): Long {
        val cb = em.criteriaBuilder
        val query = cb.createQuery(Long::class.java)
        val rootArticle = query.from(Article::class.java)

        query.select(cb.count(rootArticle))
        applyFilter(cb, rootArticle, query, listRequest.filter)

        return em.createQuery(query).singleResult
    }

    private fun applyFilter(cb: CriteriaBuilder, root: Root<*>, query: CriteriaQuery<*>, filter: ArticleListFilter) {
        val criteria = mutableListOf<Predicate>()

        if (filter.authorId != null) {
            criteria.add(cb.equal(root.get<Long>("authorId"), filter.authorId))
        }

        if (filter.titleContains != null) {
            criteria.add(cb.like(cb.upper(root.get("title")), "%${filter.titleContains.uppercase()}%"))
        }

        query.where(*criteria.toTypedArray())
    }

}
