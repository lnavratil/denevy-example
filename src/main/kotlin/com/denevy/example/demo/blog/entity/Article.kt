package com.denevy.example.demo.blog.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "articles")
data class Article(

        @Id
        @GeneratedValue
        @Column(name = "id", columnDefinition = "BIGINT", nullable = false, unique = true)
        var id: Long = 0,

        @Column(name = "title", columnDefinition = "VARCHAR(50)", nullable = false, unique = true)
        var title: String = "",

        @Column(name = "content", columnDefinition = "TEXT", nullable = false)
        var content: String = "",

        // to set relationship use annotation @ManyToOne
        @Column(name = "authorId", columnDefinition = "INT", nullable = false)
        var authorId: Int = 0

)
