package com.denevy.example.demo.blog.exception

sealed class ArticleException(message: String, cause: Throwable? = null) : RuntimeException(message, cause) {
    class ArticleProcessException(message: String, cause: Throwable? = null) : ArticleException(message, cause)
    class ArticleNotFoundException(message: String, cause: Throwable? = null) : ArticleException(message, cause)
    class ArticleBadRequestException(message: String, cause: Throwable? = null) : ArticleException(message, cause)
}
